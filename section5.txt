\section{\sffamily\Large Lagrangian Reference Frame and Transport Effect} \label{sec5}


Environmental, atmospheric, and geophysical processes are often influenced by prevailing winds or ocean currents \citep{GGG07}. Thus, the covariance function is no longer fully symmetric.
In this situation, the idea of a Lagrangian reference frame is useful. 
\cite{GGG07} summarize  the physical justification of such a framework when ${\cal D}=\R^d$. For instance, the random rotation might represent a prevailing wind as in \cite{gupta}. It might be a westerly wind considered by \cite{haslett}, or again, it might be updated dynamically according to the current state of the atmosphere.  

Lagrangian approaches have been proposed by \cite{rodriguez1987some}. Covariance functions that are not fully symmetric can also be constructed on the basis of diffusion equations or stochastic partial differential equations, and we refer the reader to \cite{GGG07} for related references.  
The basic idea behind Lagrangian construction models is to take a $d$-dimensional velocity vector, $ \boldsymbol{V}$, that is distributed according to a probability distribution and a spatial Gaussian random field that is weakly stationary with stationary spatial covariance, $K_{{\cal D}}$. Then, the resulting space-time covariance (that is weakly stationary, but not fully symmetric) is obtained through
$$ K(\h,u) = \mathbb{E}_{\boldsymbol{V}} \bigl( K_{{\cal D}}(\h- \boldsymbol{V}u )\bigr), \qquad (\h,u) \in \R^d \times \T. $$

The analogue of this construction when ${\cal D}=\S^d$ has been recently tackled in   \cite{alegria2016dimple}: 
take a random orthogonal $(d \times d)$ matrix ${\cal R}$.  
Let $Z$ be a Gaussian process on $\S^d$ with geodesically isotropic covariance $C_{\bS}(\s_1,\s_2)=\psi_{\bS}(\theta(\s_1,\s_2))$. Define
\begin{equation} \label{eqrem}  
Y(\s,t)= Z\left ( {\cal R}^{t} \s   \right ), \qquad \s \in \S^d, t \in \T,
\end{equation}  with ${\cal R}^{t}$ denoting the $t$-th power of ${\cal R}$. %--details can be found in \cite{alegria2-dimple}.
Then, the space-time covariance function with transport effect can be expressed as 
\begin{equation*} 
 C(\s_1,\s_2,u) = \mathbb{E}_{\cal R}  \bigl( C_{\bS}(\theta({\cal R}^{u} \s_1,\s_2)) \bigr) , \qquad \s_1,\s_2 \in \S^2, \quad u \in \T.  
\end{equation*} 
The fact that the resulting covariance is still geodesically isotropic in the spatial component is a nontrivial property and is shown formally, for some specific choice of the random rotation ${\cal R}$,  in \cite{alegria2016dimple}, at least for the case of the sphere $\S^d$. Some comments are in order. The resulting field $Y$ in Equation~\eqref{eqrem} however, is not Gaussian (it is Gaussian conditional on ${\cal R}$). Also, obtaining closed forms for the associated covariance is generally difficult. \cite{alegria2016dimple} provide some special cases. 
